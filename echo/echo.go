package main

import (
	"fmt"
	"os"
)

/* func main() {
	var s, sep string
	// Careful as to how the for loop counter is initialized
	for i := 1; i < len(os.Args); i++ {
		s += sep + os.Args[i]
		sep = " "
	}
	fmt.Println(s)
} */

/* func main() {
	s, sep := "", ""
	for _, arg := range os.Args[1:] {
		s += sep + arg
		sep = " "
	}
	fmt.Println(s)
} */

/* func main() {
	fmt.Println(strings.Join(os.Args[0:], " "))
} */

func main() {

	for i, arg := range os.Args[1:] {
		fmt.Printf("%d", i)
		fmt.Println(" - " + arg)
	}

}
